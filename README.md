# Bazel Common Rules

This directory contains common Bazel rules and tools for Android Kernel builds
and possibly other Bazel based builds.

For kernel-specific rules, place them in kernel checkout's [//build/kleaf
directory](https://android.googlesource.com/kernel/build/+/master/kleaf/).
